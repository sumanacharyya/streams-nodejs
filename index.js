const express = require("express");
const zlib = require("zlib");
const fs = require("fs");
const statusMonitor = require("express-status-monitor");

const PORT = 8080;
const app = express();

app.use(statusMonitor());

app.get("/", (req, res) => {
  const stream = fs.createReadStream("./sample.txt", "utf-8");
  stream.on("data", (chunk) => res.write(chunk));
  stream.on("end", () => res.end());

  // fs.readFile("./sample.txt", (err, data) => {
  //   res.end(data);
  // });
});

app.get("/zip", (req, res) => {
  // Stream-file-Read (sample.txt) --> Zipper --> Stream-file-Write (sample.zip)
  fs.createReadStream("./sample.txt").pipe(
    zlib.createGzip().pipe(fs.createWriteStream("./sample.zip"))
  );
  res.end("Zip Created");
});

app.listen(PORT, (req, res) => {
  console.log(`App listening on port --> http://localhost:${PORT}`);
});
